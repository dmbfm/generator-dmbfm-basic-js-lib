module.exports = {

    entry: './src/<%= libraryEntryFile  %>',
    output: {
	path: __dirname + "/dist",
	filename: "buildBundle.js",
	library: 'LibTest',
	libraryTarget: 'umd'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015', 'stage-0']
                }
            }
        ]
    }
    
};
