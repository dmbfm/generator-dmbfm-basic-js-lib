module.exports = {

    entry: './src/<%= runtimeEntryFile %>',
    output: {
	path: __dirname + "/bundle",
	filename: "buildBundle.js",
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015', 'stage-0']
                }
            }
        ]
    }
    
};
