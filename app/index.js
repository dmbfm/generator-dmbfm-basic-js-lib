const Generators = require('yeoman-generator');

class AppGenerator extends Generators.Base {

	constructor(args, options) {
		super(args, options);

		this.config.save();

		this.props = {};

		this.appname = this.appname.replace(/\s+/g, '-');

	}

	initializing() {
		this.log(require('yosay')("Daniel's JS Basic Lib Project Generator"));
	}

	prompting() {


		const prompts = [
			{
				type: 'input',
				name: 'projectName',
				message: 'Project name: ',
				default: this.appname
			},
			{
				type: 'input',
				name: 'libraryEntryFile',
				message: 'JS Library entry filename: ',
				default: 'index.js'
			},
			{
				type: 'input',
				name: 'runtimeEntryFile',
				message: 'JS Runtime entry filename: ',
				default: 'index.js'
			},
			{
				type: 'list',
				name: 'preprocessor',
				message: 'Select the project language:',
				choices: ['JS (Babel)', 'TypeScript'],
				default: 'JS (Babel)'
			}
		];


		return this.prompt(prompts).then((answers) => {

			this.config.set('projectName', answers.projectName);
			this.config.set('libraryEntryFile', answers.libraryEntryFile);
			this.config.set('runtimeEntryFile', answers.runtimeEntryFile);
			this.config.set('preprocessor', answers.preprocessor);

			this.props = {
				projectName: answers.projectName,
				libraryEntryFile: answers.libraryEntryFile,
				runtimeEntryFile: answers.runtimeEntryFile,
				preprocessor: answers.preprocessor
			};

		});

	}

	writing() {
		this.fs.copyTpl(this.templatePath('package.json'), this.destinationPath('package.json'), this.props);
		this.fs.copyTpl(this.templatePath('libIndex.js'), this.destinationPath('src/' + this.props.libraryEntryFile), {});
		this.fs.copyTpl(this.templatePath('runIndex.js'), this.destinationPath('prototype/src/' + this.props.runtimeEntryFile), {});
		this.fs.copy(this.templatePath('_normalize.css'), this.destinationPath('prototype/css/vendor/normalize.css'));
		this.fs.copyTpl(this.templatePath('_index.html'), this.destinationPath('prototype/index.html'), this.props);
		//this.fs.copy(this.templatePath('_jshintrc'), this.destinationPath('.jshintrc'));
		this.fs.copy(this.templatePath('_eslintrc.json'), this.destinationPath('.eslintrc.json'));
		this.fs.copy(this.templatePath('_gitignore'), this.destinationPath('.gitignore'));
		this.fs.copyTpl(this.templatePath('webpack.config.js'), this.destinationPath('webpack.config.js'), this.props);
		this.fs.copyTpl(this.templatePath('_webpack.config.js'), this.destinationPath('prototype/webpack.config.js'), this.props);
		this.fs.copy(this.templatePath('_tern-project'), this.destinationPath('.tern-project'));
	}

	install() {
		this.npmInstall();
	}

}

module.exports = AppGenerator;
